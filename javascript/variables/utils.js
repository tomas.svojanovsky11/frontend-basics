const nameField = document.querySelector('h2 span:last-child');

export function renderUI(name) {
    nameField.textContent = name;
}
