let person = {
    name: "Tomas",
    surname: "Svojanovsky",
};

console.log(`${person.name} ${person.surname}`);

person.age = 30;

console.log(person.age);

delete person.age;

console.log(person);
