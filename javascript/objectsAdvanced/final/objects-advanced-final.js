const person = {
    firstName: "Tomas",
    lastName: "Svojanovsky",
    age: 10,
    hobbies: ["Sleeping", "Eating"],
};

Object.keys(person).forEach(key => {
    console.log(key);
})

Object.values(person).forEach(value => {
    console.log(value);
});

Object.entries(person).forEach(value => {
    console.log(value); // ???
});

const { firstName, lastName } = person;
console.log(firstName, lastName);
