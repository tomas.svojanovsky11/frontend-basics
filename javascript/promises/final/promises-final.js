const wait = (time) => new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve();
        // reject();
    }, time);
});

wait(3000)
    .then(() => {
        console.log("Oh man it is so long");
    });

// wait(3000)
//     .catch(() => {
//         console.log("Oh no it failed!!!");
//     });
