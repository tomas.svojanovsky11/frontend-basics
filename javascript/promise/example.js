const wait = (time) => new Promise((resolve) => {
    setTimeout(() => {
        resolve();
    }, time);
});

new Promise((resolve, reject) => {
    resolve();
    // reject();
}).then()
.catch();
