class Human {
    strength = 0;
    speed = 0;

    constructor(strength, speed) {
        this.strength = strength;
        this.speed = speed;
    }
}

class Warrior extends Human {
    weapon = "";
    stamina = 0;

    constructor(strength, speed, weapon, stamina) {
        super(strength, speed);
        this.weapon = weapon;
        this.stamina = stamina;
    }

    attack() {
        console.log("Attack initiated");
        this.stamina -= 3;
    }

    walk() {
        console.log("Walk initiated");
        this.stamina -= 1;
    }

    showCondition() {
        console.log("Current stats:")
        console.log(`Weapon: ${this.weapon}`)
        console.log(`Stamina: ${this.stamina}`)
    }
}

const barbarian = new Warrior(100, 50, "axe", 20);
barbarian.walk();
barbarian.attack();
barbarian.showCondition();
