import {createTodo, getTodos} from "../../utils";
import {createListItem} from "../utils";

const list = document.querySelector("ul");
const button = document.querySelector("button");
const input = document.querySelector("input");

async function run() {
    input.focus();
    input.value = "";
    button.disabled = true;

    const todos = await getTodos();

    input.addEventListener("keyup", (e) => {
        const { value } = e.target;

        // create disabled button logic
    });

    todos.forEach(todo => {
        // render todos from api
    });

    button.addEventListener("click", async () => {
        // render todo after button click
    });
}

run();
