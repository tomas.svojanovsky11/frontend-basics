import {createTodo, getTodos} from "../../utils";
import {createListItem} from "../utils";

const list = document.querySelector("ul");
const button = document.querySelector("button");
const input = document.querySelector("input");

async function run() {
    input.focus();
    input.value = "";
    button.disabled = true;

    const todos = await getTodos();

    input.addEventListener("keyup", (e) => {
        const { value } = e.target;

        button.disabled = value?.length <= 0;
    });

    todos.forEach(todo => {
       createListItem({
          todo,
          list,
       });
    });

    button.addEventListener("click", async () => {
        try {
            const value = input.value;
            const result = await createTodo({
                value,
            });

            createListItem({ todo: result, list });
            input.value = "";
        } catch (e) {
            console.error(e);
        }
    });
}

run();
