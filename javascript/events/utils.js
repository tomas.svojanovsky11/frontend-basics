import {deleteTodo} from "../utils";

async function removeTodo(li, id) {
    try {
        await deleteTodo(id);
        li.remove();
    } catch (e) {
        console.error(e);
    }
}

function createIcon(li, id) {
    const divIcon = document.createElement("div");
    divIcon.classList.add("col-1", "material-icons");
    divIcon.textContent = "clear";

    divIcon.addEventListener("click", () => removeTodo(li, id));

    return divIcon;
}

function createTodoContent(value) {
    const divValue = document.createElement("div");
    divValue.textContent = value;
    divValue.classList.add("col-11");

    return divValue
}

export function createListItem({ todo, list }) {
    const li = document.createElement("li");
    const divWrapper = document.createElement("div");
    divWrapper.classList.add("row");

    const content = createTodoContent(todo.value);
    const icon = createIcon(li, todo.id);

    divWrapper.appendChild(content);
    divWrapper.appendChild(icon);

    li.appendChild(divWrapper);

    li.classList.add("list-group-item");
    list.appendChild(li);
}