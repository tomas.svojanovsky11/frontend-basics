const person = {
    firstName: "Tomas",
    lastName: "Svojanovsky",
    age: 10,
    getName() {
        let counter = 0;

        return function() {
            counter++;

            return `${person.firstName} ${person.lastName}, count ${counter}`;
        }
    },
};

