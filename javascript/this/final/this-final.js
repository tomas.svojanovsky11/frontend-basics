const person = {
    firstName: "Tomas",
    lastName: "Svojanovsky",
    age: 10,
    getName() {
        let counter = 0;

        return () => {
            counter++;

            return `${this.firstName} ${this.lastName}, count ${counter}`;
        }
    },
};

const nameFactory = person.getName();
console.log(nameFactory());
console.log(nameFactory());
