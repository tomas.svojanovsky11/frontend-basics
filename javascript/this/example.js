const person = {
    firstName: "Tomas",
    lastName: "Svojanovsky",
    getName() {
        return `${this.firstName} ${this.lastName}`
    },
};

const fullName = person.getName();
console.log(fullName);
