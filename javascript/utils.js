export async function getPeople() {
    const response = await fetch("http://localhost:3000/users");
    return response.json();
}

export async function getTodos() {
    const response = await fetch("http://localhost:3000/todos");
    return response.json();
}

export async function createTodo(todo) {
    return fetch("http://localhost:3000/todos/", {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify(todo),
    }).then(res => res.json());
}

export async function deleteTodo(id) {
    return fetch(`http://localhost:3000/todos/${id}`, {
        method: "DELETE",
    });
}
