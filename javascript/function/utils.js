const womenSection = document.querySelector('.women');
const menSection = document.querySelector('.men');

const menCounter = menSection.querySelector('h2 span:last-child');
const womenCounter = womenSection.querySelector('h2 span:last-child');
const totalCounter = document.querySelector('.total-count span:last-child');

const menList = menSection.querySelector('ul');
const womenList = womenSection.querySelector('ul');

export function renderUI({ women, men, menSum = 0, womenSum = 0, totalSum = 0 }) {
    menCounter.textContent = menSum;
    womenCounter.textContent = womenSum;
    totalCounter.textContent = totalSum;

    renderWomen(women);
    renderMen(men);
}

function renderWomen(people) {
    people.forEach(person => {
        const li = document.createElement('li');
        li.innerText = `${person.firstName} ${person.lastName} (${person.age})`;
        womenList.append(li);
    });
}

function renderMen(people) {
    people.forEach(person => {
        const li = document.createElement('li');
        li.innerText = `${person.firstName} ${person.lastName} (${person.age})`;
        menList.append(li);
    });
}