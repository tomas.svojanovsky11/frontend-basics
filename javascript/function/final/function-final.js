import { renderUI } from "../utils";
import {getPeople} from "../../utils";

const GENDER = Object.freeze({
    MALE: 'male',
    FEMALE: 'female',
});

async function run() {
    try {
        const people = await getPeople();

        function getByGender(people, gender) {
            return people.filter(person => person.gender === gender);
        }

        function getWomen(people) {
            return getByGender(people, GENDER.FEMALE);
        }

        function getMen(people) {
            return getByGender(people, GENDER.MALE);
        }

        const women = getWomen(people);
        const men = getMen(people);

        renderUI({
            men,
            women,
            menSum: men.length,
            womenSum: women.length,
            totalSum: people.length,
        })
    } catch (e) {
        console.error(e);
    }
}

run();
