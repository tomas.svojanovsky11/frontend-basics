import { renderUI } from "../utils";
import {getPeople} from "../../utils";

const GENDER = Object.freeze({
    MALE: 'male',
    FEMALE: 'female',
});

async function run() {
    try {
        const people = await getPeople();

        /**
         * Type code here
         * Change values in renderUI function
         **/

        renderUI({
            men : [],
            women : [],
            menSum: 0,
            womenSum: 0,
            totalSum: 0,
        })
    } catch (e) {
        console.error(e);
    }
}

run();
