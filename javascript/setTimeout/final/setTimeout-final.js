console.log("First line called");

setTimeout(() => {
    console.log("I should be called after 1 sec");
}, 1000);

setTimeout(() => {
    console.log("Am I called immediately?")
}, 0);

console.log("Last line called");
