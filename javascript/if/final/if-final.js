import {getPeople} from "../../utils";
import {createElement, list} from "../utils";

const STATUS = Object.freeze({
    ONLINE: "online",
    OFFLINE: "offline",
});

async function run() {
    try {
        const people = await getPeople();

        people.forEach(person => {
            const { span, li } = createElement(person);

            if (person.status === STATUS.ONLINE) {
                span.classList.add("bg-success");
            } else if (person.status === STATUS.OFFLINE) {
                span.classList.add("bg-danger");
            }

            list.appendChild(li);
        });
    } catch (e) {
        console.error(e);
    }
}

run()
