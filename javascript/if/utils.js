export const list = document.querySelector("ul");

export function createElement(person) {
    const li = document.createElement("li");
    const span = document.createElement('span');
    span.classList.add(["badge"]);
    li.classList.add(["list-group-item"]);
    span.textContent = `${person.firstName} ${person.lastName}`;
    li.appendChild(span);

    return { span, li };
}