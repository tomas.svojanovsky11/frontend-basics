import {getPeople} from "../../utils";
import {createElement, list} from "../utils";

const STATUS = Object.freeze({
    ONLINE: "online",
    OFFLINE: "offline",
});

async function run() {
    try {
        const people = await getPeople();

        people.forEach(person => {
           const { span, li } = createElement(person);

           /*
            * Type code here
            */

           list.appendChild(li);
        });
    } catch (e) {
        console.error(e);
    }
}

run()
