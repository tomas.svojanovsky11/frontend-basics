let colors = ["red", "blue", "green", "yellow", "violet"];

for (let i = 0; i < colors.length; i++) {
    console.log(colors[i]);
}

for (let color of colors) {
    console.log(color);
}
