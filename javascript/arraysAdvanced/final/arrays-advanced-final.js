const kids = [
    {
        id: 1,
        name: "Tomas",
        age: 10,
    },
];

const adults = [
    {
        id: 2,
        name: "Josef",
        age: 18,
    },
];

const teacher = {
    id: 3,
    name: "Katerina",
    age: 25,
};

let people = [...kids, ...adults];
people.forEach(person => {
    console.log(person);
});

people = [...people, teacher];

people = people.map(person => {
    return {
        ...person,
        adult: person.age >= 18,
    };
})

const onlyAdults = people.filter(person => {
    return person.adult;
})

console.log(onlyAdults);

const totalAge = people.reduce((total, person) => {
    return total + person.age;
}, 0)

console.log(totalAge / onlyAdults.length);

