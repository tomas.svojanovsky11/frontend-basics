let colors = ["red", "blue", "green"];
console.log(colors[1]); // blue
console.log(colors[99]); // undefined
console.log(colors); // ["red", "blue", "green"]
colors.push("violet");
console.log(colors); // ["red", "blue", "green", "violet"]
colors.splice(1, 1);
console.log(colors); // ["red" "green", "violet"]

let mixed = [5, 6, "xxx"];

console.log(mixed);

